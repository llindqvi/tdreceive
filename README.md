# Tdreceive's README

## Description
Tdreceive is a script launcher that runs shell commands / scripts based on events received from Telldus Tellstick Duo.
The scripts are selected based on triggers configured in triggers.ini. See triggers.ini.template for usage example.
The software can be used, for example, to integrate Tellstick compatible remote controllers and motion sensors to Home Assistant.

## Dependencies
You'll need the following softwre to run tdreceive:

- libtelldus-core (http://download.telldus.se/TellStick/Software/telldus-core)
- python wrapper for libtelldus by David Karlsson (included in tdreceive)

## Make Tdreceive to start at computer startup
- edit /etc/rc.local
- add line (starts tdreceive into screen of specified user):
```
su - <username> -c "screen -dm -S tdreceive_startup cd <path_to_tdreceive> && <path_to_python3> tdreceive.py"
```
- output of the program can be inspected by attaching to the screen
```
screen -r
```

## Reloading configs
- Attach to the screen
- ```CTRL+C```
 to stop execution
- ```python tdreceive.py```
 to start again

