# TODO
- shortcut to reload config file

# DONE
* choose config file format: ini
* add actual config file to .gitignore
* create conditional execute class
  * event criteria
  * script to execute
* read config file contents into objects in memory
* on received event
  * filter the items whose criteria match
  * and execute them
* create template config file
* instructions to run in background
  * to start on computer reboot
  * logging of output when in background
