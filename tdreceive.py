#!/usr/bin/python
# -*- coding: UTF-8 -*-


import td
import time
from datetime import datetime
import subprocess
import os
import configparser
import argparse

class Trigger:
    def __init__(self, name):
        self.name = name
        self.command = ""
        self.conditions = dict()
        self.baseTrigger = None
        self.isAbstract = False
    
    def __str__(self):
        result = "Trigger "
        if (self.isAbstract):
            result += "(abstract) "
        result = result + self.name + ", command = \"" + self.command + "\" conditions: "
        allConditions = self.getConditions()
        for condition in allConditions:
            result = result + " " + str(condition) + " = " + allConditions[condition] 
        return result

    def isMatch(self, options):
        result = True
        allConditions = self.getConditions()
        for condition in allConditions:
            if condition in options and options[condition] == allConditions[condition]:
                pass
            else:
                result = False
                break
        return result

    def getConditions(self):
        result = dict()
        if (self.baseTrigger != None):
            result.update(self.baseTrigger.getConditions())
        result.update(self.conditions)
        return result

    def execute(self):
        print("Executing trigger " + self.name + "...")
        if (len(self.command) > 0):
            subprocess.call(self.command, shell=True)
            pass
        else:
            print("Command is empty, please specify a command for the trigger")
        print("...trigger executed.")

class Config:
    def __init__(self, fileNames):
        self.variables = dict()
        self.triggers = self.loadConfig(fileNames)

    def loadConfig(self, fileNames):
        triggers = dict()
        for fileName in fileNames:
            if (os.path.isfile(fileName) == False):
                continue
            config = configparser.ConfigParser()
            config.read(fileName)
            for section in config.sections():
                if (section == "Variables"):
                    for key in config[section]:
                        value = config[section][key]
                        for variable in self.variables:
                            value = value.replace("{" + variable + "}", self.variables[variable])
                        self.variables[key] = value
                else:
                    trigger = Trigger(section)
                    triggers[section] = trigger

                    for key in config[section]:
                        value = config[section][key]
                        if key == "command" or key == "Command":
                            trigger.command = value
                            for variable in self.variables:
                                trigger.command = trigger.command.replace("{" + variable + "}", self.variables[variable])
                        elif key == "abstract" and value == "True":
                            trigger.isAbstract = True
                        elif key == "basedon":
                            trigger.baseTrigger = triggers[value]
                        else:
                            trigger.conditions[key] = value
        return triggers

    def __str__(self):
        result = "Config:\n\n"
        result += "Variables:\n"
        result += str(self.variables)
        result += "\n\nTriggers:\n"
        for trigger in self.getTriggerList():
            result = result + "  " + str(trigger) + "\n"
        return result

    def getTriggerList(self):
        return self.triggers.values()

    def getTriggers(self, options):
        result = list()
        for trigger in self.getTriggerList():
            if (trigger.isAbstract == False and trigger.isMatch(options) == True):
                result.append(trigger)
        return result

def rawDeviceEventHandler(data, controllerId, callbackId):
    global lastEventTime, config
    timeNow = time.time()
    if (timeNow - lastEventTime) > 1:

        lastEventTime = timeNow
        optionStrings = data.split(';')

        # collect options
        options = {}
        printEvent = True
        for option in optionStrings:
             parts = option.split(':')
             key = parts[0]
             if len(parts) > 1:
                 value = parts[1]
                 options[key] = value
    
        if printEvent == True:
            print str(datetime.now()) + ": RawDeviceEvent: " + str(data)

        matchingTriggers = config.getTriggers(options)
        if (len(matchingTriggers) > 0):
             print("Firing " + str(len(matchingTriggers)) + " triggers:")
        for trigger in matchingTriggers:
            trigger.execute()
           
if __name__ == '__main__':
    global lastEventTime, config
    argParser = argparse.ArgumentParser(description='Convert events from Tellstick Duo into commands')
    argParser.add_argument('configFiles', action="store", nargs="*", help="Config files in ini format. The least dependent file first. See triggers.ini.template.")
    args = argParser.parse_args()
    
    if len(args.configFiles) == 0:
        args.configFiles.append("secrets.ini")
        args.configFiles.append("triggers.ini")

    print("config files: " + str(args.configFiles))

    config = Config(args.configFiles)
    print(str(config))

    td.init(defaultMethods = td.TELLSTICK_TURNON | td.TELLSTICK_TURNOFF)

    lastEventTime = time.time()

    callbackId = []
    callbackId.append(td.registerRawDeviceEvent(rawDeviceEventHandler))

    # Main loop
    print "Press CTRL+C to exit tdreceive.py"
    try:
        while(1):
            time.sleep(1)
    except KeyboardInterrupt:
        for i in callbackId:
            td.unregisterCallback(i)
    print "tdreceive.py exited"
    td.close()
